import React, { Component } from 'react'
import propTypes from 'prop-types'

class Resume extends Component {	
	render() {
		const { 
			canvasUrl,
			height,
			width, 
		} = this.props;

		return (
			<div className="c-resume">
				<canvas src={canvasUrl} height={height} width={width}></canvas>
			</div>
		);
	}
}


Resume.propTypes = {
	canvasBase64: propTypes.string,
	height: propTypes.number,
	width: propTypes.number,
}

export default Resume;