import React from 'react';
import ReactDOM from 'react-dom';
import Resume from '../';


describe('Canvas', () => {
	let div = null;

	beforeEach(() => {
		div = document.createElement('div');
	});

	afterEach(() => {
		ReactDOM.unmountComponentAtNode(div);
	});


	it('Should element contain canvas', () => {
  	ReactDOM.render(<Resume canvasUrl="dsds" height={300} width={400} />, div);
	
  	console.log(div.innerHTML)
	});
});
